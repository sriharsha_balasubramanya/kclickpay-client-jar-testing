package com.freecharge.ims.migration.core;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.testng.Assert;

import com.freecharge.core.Testing_Environment;

public class FC_Old_UserSignUpGenerator {

	private  static HttpResponse UserSignUp_oldAPI(RequestUtil requestUtil, String Url, String firstName, String email, String password, String confirmPassword, String mobileNo) throws Exception, IOException {
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(Testing_Environment.Test_Environment + "/rest/session/create");
		Assert.assertEquals(httpResponse.getStatusLine().getStatusCode(), 200, "Response Status code for the Status Request is :" + httpResponse.getStatusLine().getStatusCode());
		JSONObject responseJson = requestUtil.getJSONObjectForResponse(httpResponse);
		String csrfRequestIdentifier = responseJson.getString("csrfRequestIdentifier");
		Assert.assertNotNull(csrfRequestIdentifier, "csrfRequestIdentifier for the Status Request is coming Null...");
		List<NameValuePair> Loginparams = new ArrayList<NameValuePair>();
		Loginparams.add(new BasicNameValuePair("firstName", firstName));
		Loginparams.add(new BasicNameValuePair("email", email));
		Loginparams.add(new BasicNameValuePair("password", password));
		Loginparams.add(new BasicNameValuePair("confirmPassword", confirmPassword));
		Loginparams.add(new BasicNameValuePair("mobileNo", mobileNo));
		Loginparams.add(new BasicNameValuePair("csrfRequestIdentifier", csrfRequestIdentifier));
		Loginparams.add(new BasicNameValuePair("affiliateId", "11"));

		// Post request to the URL with the param data
		HttpResponse httpResponseForFetchAndPay = requestUtil.postRequest(Url, null, Loginparams);
		requestUtil = null;
		return httpResponseForFetchAndPay;
	}

	public  static JSONObject UserLogin_oldAPI(RequestUtil requestUtil,String env, String email, String password) throws Exception, IOException {
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(env + "/rest/session/create");
		String url =env+"/rest/captchalogin";
		Assert.assertEquals(httpResponse.getStatusLine().getStatusCode(), 200, "Response Status code for the Status Request is :" + httpResponse.getStatusLine().getStatusCode());
		JSONObject responseJson = requestUtil.getJSONObjectForResponse(httpResponse);
		String csrfRequestIdentifier = responseJson.getString("csrfRequestIdentifier");
		Assert.assertNotNull(csrfRequestIdentifier, "csrfRequestIdentifier for the Status Request is coming Null...");
		List<NameValuePair> Loginparams = new ArrayList<NameValuePair>();
	
		Loginparams.add(new BasicNameValuePair("email", email));
		Loginparams.add(new BasicNameValuePair("password", password));
		Loginparams.add(new BasicNameValuePair("rememberme", "true"));
		
		// Post request to the URL with the param data
		HttpResponse httpResponseForFetchAndPay = requestUtil.postRequest(url, null, Loginparams);
		JSONObject responseJsonforLogin = requestUtil.getJSONObjectForResponse(httpResponseForFetchAndPay);
		responseJsonforLogin.put("csrfRequestIdentifier", csrfRequestIdentifier);
		requestUtil = null;
		return responseJsonforLogin;
	}
	
	public static JSONObject UserSignUp_on_Freecharge_Old_API() throws IOException, Exception {
		System.out.println("Createing New user on freecharge with old API");
		RequestUtil requestUtil = new RequestUtil();
		//String Email = "fc"+ i+"@gmail.com";
		String Email = "testAuto_Old" + requestUtil.generateUUID() + "@gmail.com";
		
		// generate the 10 Digit Mobile NO Starting from the 6
		String mobileNO = "6" + requestUtil.createRandomNumber(9);
		HttpResponse httpResponseForFetchAndPay = UserSignUp_oldAPI(requestUtil, Testing_Environment.Test_Environment + "/rest/signup", "test", Email, "password", "password", mobileNO);
		JSONObject responseJsonforLogin = requestUtil.getJSONObjectForResponse(httpResponseForFetchAndPay);
		if (!responseJsonforLogin.getString("status").equalsIgnoreCase("success")) {
			System.out.println("User Failed");
		} else {
			  String va =Email+","+mobileNO+"\n";
		    Files.write(Paths.get(System.getProperty("user.dir")+"//user.csv"), va.getBytes(), StandardOpenOption.APPEND);

			//writer.println(Email+"  "+mobileNO);
			//writer.println("The second line");
			
			responseJsonforLogin.put("FCUser_Email", Email);
			responseJsonforLogin.put("FCUser_Mobile", mobileNO);
			
			System.out.println(responseJsonforLogin.toString());
		}
		requestUtil = null;
		return responseJsonforLogin;

	}
	
	public static void main(String[] args) throws IOException, Exception
	{    
		//PrintWriter writer = new PrintWriter(System.getProperty("user.dir")+"//user.txt", "UTF-8");
		
		    //for(int i=100;i<=200;i++){
			JSONObject loggedin = FC_Old_UserSignUpGenerator.UserSignUp_on_Freecharge_Old_API();
			System.out.println(loggedin.getString("status"));
			System.out.println(loggedin.getString("FCUser_Email"));
			System.out.println(loggedin.getString("FCUser_Mobile"));
		    //}
		   // writer.close();
	}
}
