package com.freecharge.ims.migration.core;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class ParamsGenerator {
  
	public static List<NameValuePair> getSignUpParams(String email,String password,String firstName,String fcChannel,String mobileNumber,String signUpFrom)
	{
        List<NameValuePair> Loginparams = new ArrayList<NameValuePair>();
		Loginparams.add(new BasicNameValuePair("email", email));
		Loginparams.add(new BasicNameValuePair("password", password));
		Loginparams.add(new BasicNameValuePair("firstName", firstName));
		Loginparams.add(new BasicNameValuePair("fcChannel", fcChannel));
		Loginparams.add(new BasicNameValuePair("mobileNumber", mobileNumber));
		Loginparams.add(new BasicNameValuePair("signUpFrom", signUpFrom));
	    return Loginparams;
	}
	
	public static List<NameValuePair> getSignUpParams(String email,
			String password,String firstName,String middleName,String lastName,String fcChannel,String mobileNumber,String signUpFrom)
	{
        List<NameValuePair> Loginparams = new ArrayList<NameValuePair>();
		Loginparams.add(new BasicNameValuePair("email", email));
		Loginparams.add(new BasicNameValuePair("password", password));
		Loginparams.add(new BasicNameValuePair("firstName", firstName));
		Loginparams.add(new BasicNameValuePair("middleName", middleName));
		Loginparams.add(new BasicNameValuePair("lastName", lastName));
		Loginparams.add(new BasicNameValuePair("fcChannel", fcChannel));
		Loginparams.add(new BasicNameValuePair("mobileNumber", mobileNumber));
		Loginparams.add(new BasicNameValuePair("signUpFrom", signUpFrom));
	    return Loginparams;
	}
	
	public static List<NameValuePair> getSignIn_with_Email_Params(String email,String password,String imei,String deviceId,String fcChannel)
	{
        List<NameValuePair> signinparams = new ArrayList<NameValuePair>();
        signinparams.add(new BasicNameValuePair("email", email));
        signinparams.add(new BasicNameValuePair("password", password));
        signinparams.add(new BasicNameValuePair("imei", imei));
        signinparams.add(new BasicNameValuePair("deviceId", deviceId));
        signinparams.add(new BasicNameValuePair("fcChannel", fcChannel));
	    return signinparams;
	}
	
	public static List<NameValuePair> getGererate_OTP_Params(String mobileNumber,String email,String purpose)
	{
        List<NameValuePair> GererateOTPparams = new ArrayList<NameValuePair>();
        GererateOTPparams.add(new BasicNameValuePair("mobileNumber", mobileNumber));
        GererateOTPparams.add(new BasicNameValuePair("email", email));
        GererateOTPparams.add(new BasicNameValuePair("purpose", purpose));

	    return GererateOTPparams;
	}
	
	public static List<NameValuePair> getUpgrade_User_Params(String otpId,String otp,String upgradeSrc,String fcChannel)
	{
        List<NameValuePair> upgradeUserparams = new ArrayList<NameValuePair>();
        upgradeUserparams.add(new BasicNameValuePair("otpId", otpId));
        upgradeUserparams.add(new BasicNameValuePair("otp", otp));
        upgradeUserparams.add(new BasicNameValuePair("upgradeSrc", upgradeSrc));
        upgradeUserparams.add(new BasicNameValuePair("fcChannel", fcChannel));

	    return upgradeUserparams;
	}
	public static List<NameValuePair> getReset_Password_Params(String esource,String newPassword,String confirmPassword)
	{
        List<NameValuePair> resetPasswordparams = new ArrayList<NameValuePair>();
        resetPasswordparams.add(new BasicNameValuePair("esource", esource));
        resetPasswordparams.add(new BasicNameValuePair("password", newPassword));
        resetPasswordparams.add(new BasicNameValuePair("confirmPassword", confirmPassword));

	    return resetPasswordparams;
	}
	
	public static List<NameValuePair> getReset_Password_OTP_Params(String otpId,String otp,String fcWalletId,String password,String confirmPassword)
	{
        List<NameValuePair> resetPassword_OTP_params = new ArrayList<NameValuePair>();
        resetPassword_OTP_params.add(new BasicNameValuePair("otpId", otpId));
        resetPassword_OTP_params.add(new BasicNameValuePair("otp", otp));
        resetPassword_OTP_params.add(new BasicNameValuePair("fcWalletId", fcWalletId));
        resetPassword_OTP_params.add(new BasicNameValuePair("password", password));
        resetPassword_OTP_params.add(new BasicNameValuePair("confirmPassword", confirmPassword));

	    return resetPassword_OTP_params;
	}
	public static List<NameValuePair> getUpdate_Mobile_Params(String mobileNumber,String otpId,String otp)
	{
        List<NameValuePair> updateMobileParams = new ArrayList<NameValuePair>();
        updateMobileParams.add(new BasicNameValuePair("mobileNumber", mobileNumber));
        updateMobileParams.add(new BasicNameValuePair("otpId", otpId));
        updateMobileParams.add(new BasicNameValuePair("otp", otp));

	    return updateMobileParams;
	}
	
	public static List<NameValuePair> getReSend_OTP_Params(String otpId)
	{
        List<NameValuePair> resendOTPparams = new ArrayList<NameValuePair>();
        resendOTPparams.add(new BasicNameValuePair("otpId", otpId));
     
	    return resendOTPparams;
	}
	
	public static List<NameValuePair> getValidate_OTP_Params(String otpId,String otp)
	{
        List<NameValuePair> validateOTPparams = new ArrayList<NameValuePair>();
        validateOTPparams.add(new BasicNameValuePair("otpId", otpId));
        validateOTPparams.add(new BasicNameValuePair("otp", otp));
     
	    return validateOTPparams;
	}
	
	public static List<NameValuePair> getVerifyUserMobile_Params(String otpId,String otp,String fcChannel)
	{
        List<NameValuePair> verifyUserMobileparams = new ArrayList<NameValuePair>();
        verifyUserMobileparams.add(new BasicNameValuePair("otpId", otpId));
        verifyUserMobileparams.add(new BasicNameValuePair("otp", otp));
        verifyUserMobileparams.add(new BasicNameValuePair("fcChannel", fcChannel));
	    return verifyUserMobileparams;
	}
	
	public static List<NameValuePair> getValidate_Password_Params(String password)
	{
        List<NameValuePair> validatePasswordparams = new ArrayList<NameValuePair>();
        validatePasswordparams.add(new BasicNameValuePair("password", password));
     
	    return validatePasswordparams;
	}
	
	public static List<NameValuePair> getForgot_Password_Params(String email)
	{
        List<NameValuePair> forgotPasswordparams = new ArrayList<NameValuePair>();
        forgotPasswordparams.add(new BasicNameValuePair("email", email));
     
	    return forgotPasswordparams;
	}
	public static List<NameValuePair> getForgot_Password_OTP_Params(String email)
	{
        List<NameValuePair> forgotPasswordOTPparams = new ArrayList<NameValuePair>();
        forgotPasswordOTPparams.add(new BasicNameValuePair("email", email));
     
	    return forgotPasswordOTPparams;
	}
	public static List<NameValuePair> getUserName_Update_Params(String firstName,String middleName,String lastName)
	{
        List<NameValuePair> updateuserparams = new ArrayList<NameValuePair>();
        updateuserparams.add(new BasicNameValuePair("firstName", firstName));
        updateuserparams.add(new BasicNameValuePair("middleName", middleName));
        updateuserparams.add(new BasicNameValuePair("lastName", lastName));

	    return updateuserparams;
	}
	
	public static List<NameValuePair> getChange_Password_Params(String password,String newPassword)
	{
        List<NameValuePair> changepasswordparams = new ArrayList<NameValuePair>();
        changepasswordparams.add(new BasicNameValuePair("password", password));
        changepasswordparams.add(new BasicNameValuePair("newPassword", newPassword));

	    return changepasswordparams;
	}
	public static List<NameValuePair> getSocial_SignIn_Create_User_Params(String email,String socialSrc,String socialToken,String socialCode,
			String socialError,String imei,String deviceId,String fcChannel)
	{
        List<NameValuePair> signinparams = new ArrayList<NameValuePair>();
        signinparams.add(new BasicNameValuePair("email", email));
        signinparams.add(new BasicNameValuePair("socialSrc", socialSrc));
        signinparams.add(new BasicNameValuePair("socialToken", socialToken));
        signinparams.add(new BasicNameValuePair("socialCode", socialCode));
        signinparams.add(new BasicNameValuePair("socialError", socialError));
        signinparams.add(new BasicNameValuePair("imei", imei));
        signinparams.add(new BasicNameValuePair("deviceId", deviceId));
        signinparams.add(new BasicNameValuePair("fcChannel", fcChannel));
	    return signinparams;
	}
}
