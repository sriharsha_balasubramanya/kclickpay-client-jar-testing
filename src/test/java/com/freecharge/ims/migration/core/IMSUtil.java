package com.freecharge.ims.migration.core;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import org.testng.Assert;

import com.freecharge.core.IMSpathConstants;
import com.freecharge.core.Testing_Environment;
import com.freecharge.core.WalletEnvironment;


/**
 * Wallet util class is used to hit the URl and all the util methods used for the Testing API.
 * @author pawangaria
 *
 */
public class IMSUtil {
	
	public static JSONObject createNewUserOnServer(RequestUtil requestUtil) throws Exception {
		
		JSONObject responseJson_verifyUser = null;
		String RequstUrlCreateUser = WalletEnvironment.Test_Environment + IMSpathConstants.CREATE_USER_WITH_EMAIL;
		String RequstUrl = WalletEnvironment.Test_Environment + IMSpathConstants.CREATE_SESSION_PATH;
		String RequstUrl_VerifyUser = WalletEnvironment.Test_Environment + IMSpathConstants.VERIFY_USER;
		
		if (Testing_Environment.Env.equalsIgnoreCase("QA"))
		{
			// Get request for the session creation first.
			HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
			Assert.assertEquals(httpResponse.getStatusLine().getStatusCode(), 200, "Response Status code for the Status Request is :" + httpResponse.getStatusLine().getStatusCode());
	    	JSONObject responseJson = requestUtil.getJSONObjectForResponse(httpResponse);
			List<NameValuePair> Loginparams = ParamsGenerator.getSignUpParams("testAuto" + requestUtil.generateUUID() +Thread.currentThread().getId()+ "@gmail.com", "password", "Automation", "11", "6" + requestUtil.createRandomNumber(9), "SIGN_UP");
			// Post request to the URL with the param data
			HttpResponse httpResponseForlogin = requestUtil.postJSON_Request(RequstUrlCreateUser, responseJson.getString("csrfRequestIdentifier"), Loginparams);
			JSONObject responseJson_login = requestUtil.getJSONObjectForResponse(httpResponseForlogin);
			Assert.assertNotNull(responseJson_login.getString("otpId"), "For verify User, After OTP verify, userId should not be NUll");
	
			System.out.println("After the User is Created usingg the Sign up OTP id is send to User.");
			
			List<NameValuePair> verifyUserMobileparams = ParamsGenerator.getVerifyUserMobile_Params(responseJson_login.getString("otpId"), "1234", "11");
	
			// Post request to the URL with the param data
			HttpResponse httpResponseVerifyUser = requestUtil.postJSON_Request(RequstUrl_VerifyUser, responseJson.getString("csrfRequestIdentifier"), verifyUserMobileparams);
			responseJson_verifyUser = requestUtil.getJSONObjectForResponse(httpResponseVerifyUser);
			Assert.assertNotNull(responseJson_verifyUser.getInt("userId"), "For verify User, After OTP verify, userId should not be NUll");
		}
		
		else
		{
			responseJson_verifyUser.put("email", "testfcsd02@gmail.com");
		}
		
		return responseJson_verifyUser;
	}
	
	
	public static JSONObject signInUser_WithEmail_Login(RequestUtil requestUtil,JSONObject responseJsonUserDetails) throws Exception {
		System.out.println("Sign up with the Created User........");
		String RequstUrlsignIn = WalletEnvironment.Test_Environment + IMSpathConstants.SIGN_IN_USER_WITH_EMAIL;
		String RequstUrl = WalletEnvironment.Test_Environment + IMSpathConstants.CREATE_SESSION_PATH;
		
		// Get request for the session creation first.
		HttpResponse httpResponse = requestUtil.getRequest(RequstUrl);
		Assert.assertEquals(httpResponse.getStatusLine().getStatusCode(), 200, "Response Status code for the Status Request is :" + httpResponse.getStatusLine().getStatusCode());

		// Getting the csrfRequestIdentifier identifier for the session and
		JSONObject responseJson = requestUtil.getJSONObjectForResponse(httpResponse);
		String csrfRequestIdentifier = responseJson.getString("csrfRequestIdentifier");
		Assert.assertNotNull(csrfRequestIdentifier, "csrfRequestIdentifier for the Status Request is coming Null...");

		List<NameValuePair> signinparams = ParamsGenerator.getSignIn_with_Email_Params(responseJsonUserDetails.getString("email"), "password", null, null,"11"); //FOR QA		

		// Post request to the URL with the param data
		HttpResponse httpResponseForsignin = requestUtil.postJSON_Request(RequstUrlsignIn, csrfRequestIdentifier, signinparams);
		String cookies = httpResponseForsignin.getFirstHeader("Set-Cookie").getValue();
		String sessionApp_fc= requestUtil.getSession(cookies);
		System.out.println("appfc="+sessionApp_fc);
		JSONObject responseJson_signin = requestUtil.getJSONObjectForResponse(httpResponseForsignin);
		responseJson_signin.put("csrfRequestIdentifier",csrfRequestIdentifier);
		responseJson_signin.put("app_fc",sessionApp_fc);
		return responseJson_signin;
	}
	
	
	
}
