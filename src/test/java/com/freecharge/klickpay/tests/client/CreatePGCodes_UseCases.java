package com.freecharge.klickpay.tests.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.node.BigIntegerNode;
import com.freecharge.core.Testing_Environment;
import com.freecharge.core.Util;
import com.freecharge.core.KlickPayCodeConstants;
import com.freecharge.ims.migration.core.IMSUtil;
import com.freecharge.ims.migration.core.RequestUtil;
import com.freecharge.ims.migration.core.Results;
import com.freecharge.klickpay.client.KlickpayClient;
import com.freecharge.klickpay.vo.CardBinRangeVO;
import com.freecharge.klickpay.vo.MerchantConfigVO;
import com.freecharge.klickpay.vo.MerchantDetailsVO;
import com.freecharge.klickpay.vo.MerchantOptionsVO;
import com.freecharge.klickpay.vo.MerchantPGConfigVO;
import com.freecharge.klickpay.vo.MerchantSCConfigVO;
import com.freecharge.klickpay.vo.MerchantVO;
import com.freecharge.klickpay.vo.PGCodesVO;
import com.freecharge.klickpay.vo.PanelGenericResponse;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class CreatePGCodes_UseCases  {
	
	
	KlickpayClient client = new KlickpayClient("https://pay-dev.freecharge.in", 50000, 50000, "FC123", "AFADXCZA");
	Results results = new Results();
	RequestUtil requtil = new RequestUtil();
	 
	@Test(enabled  = true)
	public void createpgcodes_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		PGCodesVO pgcodes = new PGCodesVO();
		pgcodes.setCardType("VISA");
		pgcodes.setIsActive(true);
		pgcodes.setPaymentTypeCode("AMEX");
		pgcodes.setPgBankCode("axis123");
		pgcodes.setPgCode("cca");
		pgcodes.setPgItemCode("12345abcd");
		
		PanelGenericResponse resp = client.createPGCodes(pgcodes, "QClQi0XM");
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createpgcodes_blank_cardtype_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		PGCodesVO pgcodes = new PGCodesVO();
		pgcodes.setCardType("");
		pgcodes.setIsActive(true);
		pgcodes.setPaymentTypeCode("DINR");
		pgcodes.setPgBankCode("axis123");
		pgcodes.setPgCode("bdk");
		pgcodes.setPgItemCode("12345abcd");
		
		PanelGenericResponse resp = client.createPGCodes(pgcodes, "QClQi0XM");
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Empty card type provided.", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createpgcodes_invalid_cardtype_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		PGCodesVO pgcodes = new PGCodesVO();
		pgcodes.setCardType("TESTINVALID");
		pgcodes.setIsActive(true);
		pgcodes.setPaymentTypeCode("ATM");
		pgcodes.setPgBankCode("axis123");
		pgcodes.setPgCode("icici");
		pgcodes.setPgItemCode("12345abcd");
		
		PanelGenericResponse resp = client.createPGCodes(pgcodes, "QClQi0XM");
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createpgcodes_blank_paymenttypecode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		PGCodesVO pgcodes = new PGCodesVO();
		pgcodes.setCardType("VISA");
		pgcodes.setIsActive(true);
		pgcodes.setPaymentTypeCode("");
		pgcodes.setPgBankCode("axis123");
		pgcodes.setPgCode("payu");
		pgcodes.setPgItemCode("12345abcd");
		
		PanelGenericResponse resp = client.createPGCodes(pgcodes, "QClQi0XM");
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Empty payment type provided", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createpgcodes_invalid_paymenttypecode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		PGCodesVO pgcodes = new PGCodesVO();
		pgcodes.setCardType("VISA");
		pgcodes.setIsActive(true);
		pgcodes.setPaymentTypeCode("testinvalid");
		pgcodes.setPgBankCode("axis123");
		pgcodes.setPgCode("payu");
		pgcodes.setPgItemCode("12345abcd");
		
		PanelGenericResponse resp = client.createPGCodes(pgcodes, "QClQi0XM");
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createpgcodes_blank_pgbankcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		PGCodesVO pgcodes = new PGCodesVO();
		pgcodes.setCardType("VISA");
		pgcodes.setIsActive(true);
		pgcodes.setPaymentTypeCode("NB"); 
		pgcodes.setPgBankCode(""); 
		pgcodes.setPgCode("ocw");
		pgcodes.setPgItemCode("12345abcd");
		
		PanelGenericResponse resp = client.createPGCodes(pgcodes, "QClQi0XM");
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Empty pg bank code provided", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createpgcodes_blank_pgcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		PGCodesVO pgcodes = new PGCodesVO();
		pgcodes.setCardType("VISA");
		pgcodes.setIsActive(true);
		pgcodes.setPaymentTypeCode("DC");
		pgcodes.setPgBankCode("axis123");
		pgcodes.setPgCode("");
		pgcodes.setPgItemCode("12345abcd");
		
		PanelGenericResponse resp = client.createPGCodes(pgcodes, "QClQi0XM");
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Empty pgcode provided", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void createpgcodes_invalid_pgcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		PGCodesVO pgcodes = new PGCodesVO();
		pgcodes.setCardType("VISA");
		pgcodes.setIsActive(true);
		pgcodes.setPaymentTypeCode("DC");
		pgcodes.setPgBankCode("axis123");
		pgcodes.setPgCode("invalidpg");
		pgcodes.setPgItemCode("12345abcd");
		
		PanelGenericResponse resp = client.createPGCodes(pgcodes, "QClQi0XM");
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createpgcodes_blank_pgitemcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		PGCodesVO pgcodes = new PGCodesVO();
		pgcodes.setCardType("VISA");
		pgcodes.setIsActive(true);
		pgcodes.setPaymentTypeCode("CC");
		pgcodes.setPgBankCode("axis123");
		pgcodes.setPgCode("ctp");
		pgcodes.setPgItemCode("");
		
		PanelGenericResponse resp = client.createPGCodes(pgcodes, "QClQi0XM");
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Empty pg item code provided", "Status Message is not displayed");
		
	}
	
	@AfterTest
	public void Aftertest(){
		
		results.extreports.flush();
	}
	
}