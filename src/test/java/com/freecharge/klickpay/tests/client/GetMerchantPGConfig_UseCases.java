package com.freecharge.klickpay.tests.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.node.BigIntegerNode;
import com.freecharge.core.Testing_Environment;
import com.freecharge.core.Util;
import com.freecharge.core.KlickPayCodeConstants;
import com.freecharge.ims.migration.core.IMSUtil;
import com.freecharge.ims.migration.core.RequestUtil;
import com.freecharge.ims.migration.core.Results;
import com.freecharge.klickpay.client.KlickpayClient;
import com.freecharge.klickpay.vo.MerchantConfigVO;
import com.freecharge.klickpay.vo.MerchantDetailsVO;
import com.freecharge.klickpay.vo.MerchantInfoVO;
import com.freecharge.klickpay.vo.MerchantOptionsVO;
import com.freecharge.klickpay.vo.MerchantPGConfigVO;
import com.freecharge.klickpay.vo.MerchantSCConfigVO;
import com.freecharge.klickpay.vo.MerchantVO;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class GetMerchantPGConfig_UseCases  {
	
	
	KlickpayClient client = new KlickpayClient("https://pay-dev.freecharge.in", 50000, 50000, "FC123", "AFADXCZA");
	Results results = new Results();
	RequestUtil requtil = new RequestUtil();
	
	@Test(enabled  = true)
	public void getmerchantPGconfig_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		 
		List<MerchantPGConfigVO> merchantpgconfiglist = client.getMerchantPGMapping("bpX0usEr");
		
		for (int i=0;i<merchantpgconfiglist.size(); i++){
		results.assertnotnull(merchantpgconfiglist.get(i).getPgName(),  "PG name is not empty");
		results.assertnotnull(merchantpgconfiglist.get(i).getPgMerchantCode(), "PG Merchant code is not empty");
		results.assertnotnull(merchantpgconfiglist.get(i).getPgMerchantKey(), "PG Merchant key is not empty");
		results.assertnotnull(merchantpgconfiglist.get(i).getPgMerchantSecurityId(), "PG ,merchant Secret is not empty");
		}
	}
	
	@Test(enabled  = true)
	public void getmerchantPGconfig_invalid_merchantcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		
		List<MerchantPGConfigVO> merchantpgconfiglist = client.getMerchantPGMapping("invalidcode");
		
		for (int i=0;i<merchantpgconfiglist.size(); i++){
		results.assertequals(merchantpgconfiglist.get(i).getStatus(),  "E500", "Status Code is not displayed properly");
		results.assertequals(merchantpgconfiglist.get(i).getStatusMssg(), "Invalid Merchant Code", "Status Message is not displayed properly");
		}
	}
	
	@Test(enabled  = true)
	public void getmerchantPGconfig_blank_merchantcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		
		List<MerchantPGConfigVO> merchantpgconfiglist = client.getMerchantPGMapping("");
		
		for (int i=0;i<merchantpgconfiglist.size(); i++){
		results.assertequals(merchantpgconfiglist.get(i).getStatus(),  "E500", "Status Code is not displayed properly");
		results.assertequals(merchantpgconfiglist.get(i).getStatusMssg(), "Invalid Merchant Code", "Status Message is not displayed properly");
		}
	}
	
	
	@AfterTest
	public void Aftertest(){
		
		results.extreports.flush();
	}
	
}