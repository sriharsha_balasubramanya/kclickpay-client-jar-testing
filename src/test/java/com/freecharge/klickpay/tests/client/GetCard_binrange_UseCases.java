package com.freecharge.klickpay.tests.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.node.BigIntegerNode;
import com.freecharge.core.Testing_Environment;
import com.freecharge.core.Util;
import com.freecharge.core.KlickPayCodeConstants;
import com.freecharge.ims.migration.core.IMSUtil;
import com.freecharge.ims.migration.core.RequestUtil;
import com.freecharge.ims.migration.core.Results;
import com.freecharge.klickpay.client.KlickpayClient;
import com.freecharge.klickpay.vo.CardBinRangeVO;
import com.freecharge.klickpay.vo.MerchantConfigVO;
import com.freecharge.klickpay.vo.MerchantDetailsVO;
import com.freecharge.klickpay.vo.MerchantInfoVO;
import com.freecharge.klickpay.vo.MerchantOptionsVO;
import com.freecharge.klickpay.vo.MerchantPGConfigVO;
import com.freecharge.klickpay.vo.MerchantSCConfigVO;
import com.freecharge.klickpay.vo.MerchantVO;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class GetCard_binrange_UseCases  {
	
	
	KlickpayClient client = new KlickpayClient("https://pay-dev.freecharge.in", 50000, 50000, "FC123", "AFADXCZA");
	Results results = new Results();
	RequestUtil requtil = new RequestUtil();
	 
	@Test(enabled  = true)
	public void getcardbinrange_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		
		CardBinRangeVO cardbinrange = client.getCardBinRange("421388");
		
		System.out.println(cardbinrange);
		results.assertnotnull(cardbinrange.getBinHead(), "Bin head is not empty");
		results.assertnotnull(cardbinrange.getBinTail(), "Bin tail is not empty");
		/*results.assertnotnull(cardbinrange.getIssuingBank(), "Issuing bank is not empty");
		results.assertnotnull(cardbinrange.getRoutingSequence(), "Routing sequence is not empty");*/
		results.assertnotnull(cardbinrange.getCardType(), "Card type is not empty");
		results.assertnotnull(cardbinrange.getCardNature(), "Card nature is not empty");
	}
	
	@Test(enabled  = true)
	public void getcardbinrange_blank_binhead_FailureCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		
		CardBinRangeVO cardbinrange = client.getCardBinRange("");
		
		System.out.println(cardbinrange);
		results.assertequals(cardbinrange.getStatus(), "E500", "Bin head is not empty");
		results.assertequals(cardbinrange.getStatusMssg(), "Invalid or empty input provided.", "Bin tail is not empty");
	}
	
	@Test(enabled  = true)
	public void getcardbinrange_invalid_binhead_FailureCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		String binhead = "478555";
		CardBinRangeVO cardbinrange = client.getCardBinRange(binhead);
		
		System.out.println(cardbinrange);
		results.assertequals(cardbinrange.getStatus(), "E500", "Bin head is not empty");
		results.assertequals(cardbinrange.getStatusMssg(), "No data found for given binHead: "+binhead+"", "Status message is not empty");
	}
	
	
	@AfterTest
	public void Aftertest(){
		
		results.extreports.flush();
	}
	
}