package com.freecharge.klickpay.tests.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.node.BigIntegerNode;
import com.freecharge.core.Testing_Environment;
import com.freecharge.core.Util;
import com.freecharge.core.KlickPayCodeConstants;
import com.freecharge.ims.migration.core.IMSUtil;
import com.freecharge.ims.migration.core.RequestUtil;
import com.freecharge.ims.migration.core.Results;
import com.freecharge.klickpay.client.KlickpayClient;
import com.freecharge.klickpay.vo.MerchantConfigVO;
import com.freecharge.klickpay.vo.MerchantDetailsVO;
import com.freecharge.klickpay.vo.MerchantOptionsVO;
import com.freecharge.klickpay.vo.MerchantPGConfigVO;
import com.freecharge.klickpay.vo.MerchantSCConfigVO;
import com.freecharge.klickpay.vo.MerchantVO;
import com.freecharge.klickpay.vo.PanelGenericResponse;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class CreateMerchantConfig_UseCases  {
	
	
	KlickpayClient client = new KlickpayClient("https://pay-dev.freecharge.in", 50000, 50000, "FC123", "AFADXCZA");
	Results results = new Results();
	RequestUtil requtil = new RequestUtil();
	
	@Test(enabled  = true)
	public void createmerchant_config_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantConfigVO merchantconfigvo = new MerchantConfigVO();
		merchantconfigvo.setPaymentType("NB");
		merchantconfigvo.setCardType("VI");
		merchantconfigvo.setPrimaryPG("icici");
		merchantconfigvo.setSecondaryPG("hdfc");
		
		PanelGenericResponse resp = client.createMerchantConfig(merchantconfigvo, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_config_forall_paymenttype_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		for(int i=0; i<KlickPayCodeConstants.PAYMENT_TYPE_CODES.length; i++){
		MerchantConfigVO merchantconfigvo = new MerchantConfigVO();
		merchantconfigvo.setPaymentType(KlickPayCodeConstants.PAYMENT_TYPE_CODES[i]);
		if(KlickPayCodeConstants.PAYMENT_TYPE_CODES[i].equalsIgnoreCase("AMEX")){
		merchantconfigvo.setCardType("AMEX");
		}else if(KlickPayCodeConstants.PAYMENT_TYPE_CODES[i].equalsIgnoreCase("DINR")){
		merchantconfigvo.setCardType("DINR");
		}else{
		merchantconfigvo.setCardType("VI");
		}
		merchantconfigvo.setPrimaryPG("icici");
		merchantconfigvo.setSecondaryPG("hdfc");
		
		PanelGenericResponse resp = client.createMerchantConfig(merchantconfigvo, "QClQi0XM");
		System.out.println(resp.getStatus()+","+resp.getStatusMssg());
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		}
	}
	
	@Test(enabled  = true)
	public void createmerchant_config_forall_cardttype_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		for(int i=0; i<KlickPayCodeConstants.CARD_TYPE_CODES.length; i++){
		MerchantConfigVO merchantconfigvo = new MerchantConfigVO();
		merchantconfigvo.setPaymentType("NB");
		merchantconfigvo.setCardType(KlickPayCodeConstants.CARD_TYPE_CODES[i]);
		merchantconfigvo.setPrimaryPG("icici");
		merchantconfigvo.setSecondaryPG("hdfc");
		
		PanelGenericResponse resp = client.createMerchantConfig(merchantconfigvo, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		}
	}
	
	@Test(enabled  = true)
	public void createmerchant_config_forall_pgtype_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		int cnt = KlickPayCodeConstants.PG_CODES.length;
		for(int i=0; i<KlickPayCodeConstants.PG_CODES.length; i++){
		MerchantConfigVO merchantconfigvo = new MerchantConfigVO();
		merchantconfigvo.setPaymentType("NB");
		merchantconfigvo.setCardType("MAES");
		merchantconfigvo.setPrimaryPG(KlickPayCodeConstants.PG_CODES[i]);
		merchantconfigvo.setSecondaryPG(KlickPayCodeConstants.PG_CODES[cnt -(i+1)]);
		
		PanelGenericResponse resp = client.createMerchantConfig(merchantconfigvo, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		}
	}
	
	
	
	@Test(enabled  = true)
	public void createmerchant_config_blank_paymenttype_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantConfigVO merchantconfigvo = new MerchantConfigVO();
		merchantconfigvo.setPaymentType("");
		merchantconfigvo.setCardType("VI");
		merchantconfigvo.setPrimaryPG("icici"); 
		merchantconfigvo.setSecondaryPG("hdfc");
		
		PanelGenericResponse resp = client.createMerchantConfig(merchantconfigvo, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createmerchant_config_invalid_paymenttype_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantConfigVO merchantconfigvo = new MerchantConfigVO();
		merchantconfigvo.setPaymentType("abcd");
		merchantconfigvo.setCardType("VI");
		merchantconfigvo.setPrimaryPG("icici");
		merchantconfigvo.setSecondaryPG("hdfc");
		
		PanelGenericResponse resp = client.createMerchantConfig(merchantconfigvo, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
		
	}
	
	
	@Test(enabled  = true)
	public void createmerchant_config_blank_primarypg_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantConfigVO merchantconfigvo = new MerchantConfigVO();
		merchantconfigvo.setPaymentType("CC");
		merchantconfigvo.setCardType("VI");
		merchantconfigvo.setPrimaryPG("");
		merchantconfigvo.setSecondaryPG("hdfc");
		
		PanelGenericResponse resp = client.createMerchantConfig(merchantconfigvo, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Primary PG not provided for Merchant Config", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_config_invalid_primarypg_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantConfigVO merchantconfigvo = new MerchantConfigVO();
		merchantconfigvo.setPaymentType("CC");
		merchantconfigvo.setCardType("VI");
		merchantconfigvo.setPrimaryPG("test");
		merchantconfigvo.setSecondaryPG("hdfc");
		
		PanelGenericResponse resp = client.createMerchantConfig(merchantconfigvo, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid PG : "+merchantconfigvo.getPrimaryPG()+"", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_config_blank_secondarypg_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantConfigVO merchantconfigvo = new MerchantConfigVO();
		merchantconfigvo.setPaymentType("CC");
		merchantconfigvo.setCardType("VI");
		merchantconfigvo.setPrimaryPG("hdfc");
		merchantconfigvo.setSecondaryPG("");
		
		PanelGenericResponse resp = client.createMerchantConfig(merchantconfigvo, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Secondary PG not provided for Merchant Config", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_config_invalid_secondarypg_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantConfigVO merchantconfigvo = new MerchantConfigVO();
		merchantconfigvo.setPaymentType("CC");
		merchantconfigvo.setCardType("VI");
		merchantconfigvo.setPrimaryPG("hdfc");
		merchantconfigvo.setSecondaryPG("test");
		
		PanelGenericResponse resp = client.createMerchantConfig(merchantconfigvo, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid PG : "+merchantconfigvo.getSecondaryPG()+"", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_config_invalid_merchantcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantConfigVO merchantconfigvo = new MerchantConfigVO();
		merchantconfigvo.setPaymentType("CC");
		merchantconfigvo.setCardType("VI");
		merchantconfigvo.setPrimaryPG("hdfc");
		merchantconfigvo.setSecondaryPG("test");
		
		PanelGenericResponse resp = client.createMerchantConfig(merchantconfigvo, "invalidtest");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Merchant Code", "Status Message is not displayed");
		
	}
	
	
	@AfterTest
	public void Aftertest(){
		
		results.extreports.flush();
	}
	
}