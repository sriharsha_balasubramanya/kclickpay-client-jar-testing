package com.freecharge.klickpay.tests.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.node.BigIntegerNode;
import com.freecharge.core.Testing_Environment;
import com.freecharge.core.Util;
import com.freecharge.core.KlickPayCodeConstants;
import com.freecharge.ims.migration.core.IMSUtil;
import com.freecharge.ims.migration.core.RequestUtil;
import com.freecharge.ims.migration.core.Results;
import com.freecharge.klickpay.client.KlickpayClient;
import com.freecharge.klickpay.vo.MerchantConfigVO;
import com.freecharge.klickpay.vo.MerchantDetailsVO;
import com.freecharge.klickpay.vo.MerchantOptionsVO;
import com.freecharge.klickpay.vo.MerchantPGConfigVO;
import com.freecharge.klickpay.vo.MerchantSCConfigVO;
import com.freecharge.klickpay.vo.MerchantVO;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class CreateMerchant_UseCases  {
	
	
	KlickpayClient client = new KlickpayClient("https://pay-dev.freecharge.in", 50000, 50000, "FC123", "AFADXCZA");
	Results results = new Results();
	RequestUtil requtil = new RequestUtil();
	
	@Test(enabled  = true)
	public void createmerchant_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("freecharge@009");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632554125");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(false);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("NB");
		merchantconfig.setCardType("MC");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		//System.out.println(merchantvo.);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		results.assertnotnull(resp.getMerchantCode(), "Merchant Code is not displayed as Null");
		results.assertnotnull(resp.getMerchantKey(), "Merchant Key is not displayed as Null");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_forall_paymenttype_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		for(int i=0; i<KlickPayCodeConstants.PAYMENT_TYPE_CODES.length; i++){
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("freecharge@009");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632554125");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType(KlickPayCodeConstants.PAYMENT_TYPE_CODES[i]);
		if(KlickPayCodeConstants.PAYMENT_TYPE_CODES[i].equalsIgnoreCase("AMEX")){
		merchantconfig.setCardType("AMEX");
		}else if(KlickPayCodeConstants.PAYMENT_TYPE_CODES[i].equalsIgnoreCase("DINR")){
		merchantconfig.setCardType("DINR");
		}else{
		merchantconfig.setCardType("MC");
		}
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		//System.out.println(merchantvo.);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		results.assertnotnull(resp.getMerchantCode(), "Merchant Code is not displayed as Null");
		results.assertnotnull(resp.getMerchantKey(), "Merchant Key is not displayed as Null");
		}
	}
	
	@Test(enabled  = true)
	public void createmerchant_forall_cardttype_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		for(int i=0; i<KlickPayCodeConstants.CARD_TYPE_CODES.length; i++){
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("freecharge@009");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632554125");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("DC");
		merchantconfig.setCardType(KlickPayCodeConstants.CARD_TYPE_CODES[i]);
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		//System.out.println(merchantvo.);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		results.assertnotnull(resp.getMerchantCode(), "Merchant Code is not displayed as Null");
		results.assertnotnull(resp.getMerchantKey(), "Merchant Key is not displayed as Null");
		}
	}
	
	@Test(enabled  = true)
	public void createmerchant_forall_pgtype_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		int cnt = KlickPayCodeConstants.PG_CODES.length;
		
		for(int i=0; i<KlickPayCodeConstants.PG_CODES.length; i++){
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("freecharge@009");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632554125");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("DC");
		merchantconfig.setCardType("VI");
		merchantconfig.setPrimaryPG(KlickPayCodeConstants.PG_CODES[i]);
		merchantconfig.setSecondaryPG(KlickPayCodeConstants.PG_CODES[cnt-(i+1)]);
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName(KlickPayCodeConstants.PG_CODES[i]);
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName(KlickPayCodeConstants.PG_CODES[cnt-(i+1)]);
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		//System.out.println(merchantvo.);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		results.assertnotnull(resp.getMerchantCode(), "Merchant Code is not displayed as Null");
		results.assertnotnull(resp.getMerchantKey(), "Merchant Key is not displayed as Null");
		}
	}
	
	
	@Test(enabled  = true)
	public void createmerchant_blank_merchantname_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632554125");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("DC");
		merchantconfig.setCardType("MC");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		//System.out.println(merchantvo.);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid merchant name", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	
	@Test(enabled  = true)
	public void createmerchant_blank_clientcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("");
		merchantvo.setContactNo("9632554125");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("DC");
		merchantconfig.setCardType("MC");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		//System.out.println(merchantvo.);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid client code", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_duplicate_clientcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("xyz123");
		merchantvo.setContactNo("9632554125");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("DC");
		merchantconfig.setCardType("MC");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		//System.out.println(merchantvo.);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E100", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Duplicate client code", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	
	@Test(enabled  = true)
	public void createmerchant_blank_contactno_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("DC");
		merchantconfig.setCardType("MC");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		//System.out.println(merchantvo.);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Contact No", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	
	@Test(enabled  = true)
	public void createmerchant_invalid_contactno_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("abcdef");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("DC");
		merchantconfig.setCardType("MC");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		//System.out.println(merchantvo.);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Contact No", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	
	@Test(enabled  = true)
	public void createmerchant_lessthan_7digits_contactno_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("963211");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("DC");
		merchantconfig.setCardType("MC");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Contact No", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_morethan_15digits_contactno_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879474521");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("DC");
		merchantconfig.setCardType("MC");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Contact No", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_blank_email_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879");
		merchantvo.setContactMail("");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("DC");
		merchantconfig.setCardType("MC");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Email", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_invalid_email_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879");
		merchantvo.setContactMail("test@testcom");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("DC");
		merchantconfig.setCardType("MC");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Email", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	
	@Test(enabled  = true)
	public void createmerchant_invalid_format_email_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879");
		merchantvo.setContactMail("testtest.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("DC");
		merchantconfig.setCardType("MC");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Email", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_blank_paymenttype_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("");
		merchantconfig.setCardType("VI");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid merchant name", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_invalid_paymenttype_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("ABCD");
		merchantconfig.setCardType("VI");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Error Message is not displayed");
		results.assertnotnull(resp.getMerchantCode(), "Merchant Code is not displayed as Null");
		results.assertnotnull(resp.getMerchantKey(), "Merchant Key is not displayed as Null");
		
	}
	
	
	
	@Test(enabled  = true)
	public void createmerchant_blank_Cardtype_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("DC");
		merchantconfig.setCardType("");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid card type for "+merchantconfiglist.get(0).getPaymentType()+" : "+merchantconfiglist.get(0).getCardType()+"", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_invalid_Cardtype_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879");
		merchantvo.setContactMail("test@test.com"); 
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("CC");
		merchantconfig.setCardType("ABCD");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid card type for CC : "+merchantconfig.getCardType()+"", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	
	@Test(enabled  = true)
	public void createmerchant_blank_primarypg_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("CC");
		merchantconfig.setCardType("VI");
		merchantconfig.setPrimaryPG("");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("bdk");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E101", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid PG : "+merchantconfig.getPrimaryPG()+"", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	} 
	
	@Test(enabled  = true)
	public void createmerchant_invalid_primarypg_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("CC");
		merchantconfig.setCardType("VI");
		merchantconfig.setPrimaryPG("abcd");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("abcd");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E101", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid PG : "+merchantconfig.getPrimaryPG()+"", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_invalid_secondarypg_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("CC");
		merchantconfig.setCardType("VI");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("test");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("cca");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("test");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);

		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E101", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid PG : "+merchantconfig.getSecondaryPG()+"", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	
	
	@Test(enabled  = true)
	public void createmerchant_blank_PG_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("CC");
		merchantconfig.setCardType("VI");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "No config provided for  PG : cca", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");                                   
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_invalid_PG_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("CC");
		merchantconfig.setCardType("VI");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("invalidPG");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234");
		merchantpgconfig1.setPgMerchantKey("abc123");
		merchantpgconfig1.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "No config provided for  PG : cca", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_null_merchantpgconfig_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("CC");
		merchantconfig.setCardType("VI");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "No PG Config provided", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	@Test(enabled  = true)
	public void createmerchant_duplicate_pgconfig_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("CC");
		merchantconfig.setCardType("VI");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("bdk");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("bdk");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("bdk");
		merchantpgconfig1.setPgMerchantCode("abc1234666");
		merchantpgconfig1.setPgMerchantKey("abc123666");
		merchantpgconfig1.setPgMerchantSecurityId("abcd123466666");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Duplicate config for PG : "+merchantpgconfig.getPgName()+"", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	
	@Test(enabled  = true)
	public void createmerchant_no_pgconfig_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantVO merchantvo = new MerchantVO();
		merchantvo.setMerchantName("testmerchant");
		merchantvo.setMerchantClientCode("testklickpay"+requtil.createRandomNumber(7)+"");
		merchantvo.setContactNo("9632415879");
		merchantvo.setContactMail("test@test.com");
		merchantvo.setLogo("logoone");
		
		MerchantOptionsVO merchantoptions= new MerchantOptionsVO();
		merchantoptions.setDynamicRoutingEnabled(true);
		merchantoptions.setIntlCardsAccepted(false);
		merchantvo.setMerchantOptionsVO(merchantoptions);
		
		MerchantConfigVO merchantconfig = new MerchantConfigVO();
		List<MerchantConfigVO> merchantconfiglist = new ArrayList<MerchantConfigVO>();
		merchantconfig.setPaymentType("CC");
		merchantconfig.setCardType("VI");
		merchantconfig.setPrimaryPG("cca");
		merchantconfig.setSecondaryPG("icici");
		merchantconfiglist.add(merchantconfig);
		merchantvo.setMerchantConfigVO(merchantconfiglist);
		
		MerchantPGConfigVO merchantpgconfig = new MerchantPGConfigVO();
		List<MerchantPGConfigVO> merchantpgconfiglist = new ArrayList<MerchantPGConfigVO>();
		merchantpgconfig.setPgName("bdk");
		merchantpgconfig.setPgMerchantCode("abc1234");
		merchantpgconfig.setPgMerchantKey("abc123");
		merchantpgconfig.setPgMerchantSecurityId("abcd1234");
		merchantpgconfiglist.add(merchantpgconfig);
		
		MerchantPGConfigVO merchantpgconfig1 = new MerchantPGConfigVO();
		merchantpgconfig1.setPgName("payu");
		merchantpgconfig1.setPgMerchantCode("abc1234666");
		merchantpgconfig1.setPgMerchantKey("abc123666");
		merchantpgconfig1.setPgMerchantSecurityId("abcd123466666");
		merchantpgconfiglist.add(merchantpgconfig1);
		merchantvo.setMerchantPGConfigVO(merchantpgconfiglist);
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		List<MerchantSCConfigVO> merchantpscconfiglist = new ArrayList<MerchantSCConfigVO>();
		merchantscconfig.setAccessKey("xyz123");
		merchantscconfig.setSecret("xyz123");
		merchantpscconfiglist.add(merchantscconfig);
		merchantvo.setMerchantSCConfigVO(merchantscconfig);
		
		MerchantDetailsVO resp = client.createMerchant(merchantvo);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "No config provided for  PG : "+merchantconfig.getPrimaryPG()+"", "Error Message is not displayed");
		results.assertequals(resp.getMerchantCode(), null, "Merchant Code is not displayed as Null");
		results.assertequals(resp.getMerchantKey(), null, "Merchant Key is not displayed as Null");
		
	}
	
	
	@AfterTest
	public void Aftertest(){
		
		results.extreports.flush();
	}
	
}