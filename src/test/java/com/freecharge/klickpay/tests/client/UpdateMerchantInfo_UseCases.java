package com.freecharge.klickpay.tests.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.node.BigIntegerNode;
import com.freecharge.core.Testing_Environment;
import com.freecharge.core.Util;
import com.freecharge.core.KlickPayCodeConstants;
import com.freecharge.ims.migration.core.IMSUtil;
import com.freecharge.ims.migration.core.RequestUtil;
import com.freecharge.ims.migration.core.Results;
import com.freecharge.klickpay.client.KlickpayClient;
import com.freecharge.klickpay.vo.MerchantConfigVO;
import com.freecharge.klickpay.vo.MerchantDetailsVO;
import com.freecharge.klickpay.vo.MerchantInfoVO;
import com.freecharge.klickpay.vo.MerchantOptionsVO;
import com.freecharge.klickpay.vo.MerchantPGConfigVO;
import com.freecharge.klickpay.vo.MerchantSCConfigVO;
import com.freecharge.klickpay.vo.MerchantVO;
import com.freecharge.klickpay.vo.PanelGenericResponse;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class UpdateMerchantInfo_UseCases  {
	
	
	KlickpayClient client = new KlickpayClient("https://pay-dev.freecharge.in", 50000, 50000, "FC123", "AFADXCZA");
	Results results = new Results();
	RequestUtil requtil = new RequestUtil();
	
	@Test(enabled  = true)
	public void updatemerchant_info_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("Address1");
		merchantinfo.setContactMail("test111@test.com");
		merchantinfo.setContactNumber("9632558964");
		merchantinfo.setIsDynamicRoutingEnabled("true");
		merchantinfo.setMerchantCode("jBKP07OX");
		merchantinfo.setMerchantKey("nlClVJ");
		merchantinfo.setMerchantLogo("logonext");
		merchantinfo.setMerchantName("New merchant 007");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void updatemerchant_info_blank_address_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("");
		merchantinfo.setContactMail("test111@test.com");
		merchantinfo.setContactNumber("9632558964");
		merchantinfo.setIsDynamicRoutingEnabled("true");
		merchantinfo.setMerchantCode("jBKP07OX");
		merchantinfo.setMerchantKey("nlClVJ");
		merchantinfo.setMerchantLogo("logonext");
		merchantinfo.setMerchantName("New merchant 007");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}

	@Test(enabled  = true)
	public void updatemerchant_info_blank_email_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("testaddress1");
		merchantinfo.setContactMail("");
		merchantinfo.setContactNumber("9632558964");
		merchantinfo.setIsDynamicRoutingEnabled("true");
		merchantinfo.setMerchantCode("jBKP07OX");
		merchantinfo.setMerchantKey("nlClVJ");
		merchantinfo.setMerchantLogo("logonext");
		merchantinfo.setMerchantName("New merchant 007");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Email", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void updatemerchant_info_invalid_email_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("testaddress1");
		merchantinfo.setContactMail("test@testcom");
		merchantinfo.setContactNumber("9632558964");
		merchantinfo.setIsDynamicRoutingEnabled("true");
		merchantinfo.setMerchantCode("jBKP07OX");
		merchantinfo.setMerchantKey("nlClVJ");
		merchantinfo.setMerchantLogo("logonext");
		merchantinfo.setMerchantName("New merchant 007");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Email", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void updatemerchant_info_invalid_format_email_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("testaddress1");
		merchantinfo.setContactMail("testtest.com");
		merchantinfo.setContactNumber("9632558964");
		merchantinfo.setIsDynamicRoutingEnabled("true");
		merchantinfo.setMerchantCode("jBKP07OX");
		merchantinfo.setMerchantKey("nlClVJ");
		merchantinfo.setMerchantLogo("logonext");
		merchantinfo.setMerchantName("New merchant 007");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Email", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void updatemerchant_info_blank_contactno_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("testaddress1");
		merchantinfo.setContactMail("test111@test.com");
		merchantinfo.setContactNumber("");
		merchantinfo.setIsDynamicRoutingEnabled("true");
		merchantinfo.setMerchantCode("jBKP07OX");
		merchantinfo.setMerchantKey("nlClVJ");
		merchantinfo.setMerchantLogo("logonext");
		merchantinfo.setMerchantName("New merchant 007");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Contact No", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void updatemerchant_info_invalid_contactno_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("testaddress1");
		merchantinfo.setContactMail("test111@test.com");
		merchantinfo.setContactNumber("abcdinvalid");
		merchantinfo.setIsDynamicRoutingEnabled("true");
		merchantinfo.setMerchantCode("jBKP07OX");
		merchantinfo.setMerchantKey("nlClVJ");
		merchantinfo.setMerchantLogo("logonext");
		merchantinfo.setMerchantName("New merchant 007");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Contact No", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void updatemerchant_info_lessthan_7digits_contactno_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("testaddress1");
		merchantinfo.setContactMail("test111@test.com");
		merchantinfo.setContactNumber("965477");
		merchantinfo.setIsDynamicRoutingEnabled("true");
		merchantinfo.setMerchantCode("jBKP07OX");
		merchantinfo.setMerchantKey("nlClVJ");
		merchantinfo.setMerchantLogo("logonext");
		merchantinfo.setMerchantName("New merchant 007");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Contact No", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void updatemerchant_info_morethan_15digits_contactno_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("testaddress1");
		merchantinfo.setContactMail("test111@test.com");
		merchantinfo.setContactNumber("9786541236585412");
		merchantinfo.setIsDynamicRoutingEnabled("true");
		merchantinfo.setMerchantCode("jBKP07OX");
		merchantinfo.setMerchantKey("nlClVJ");
		merchantinfo.setMerchantLogo("logonext");
		merchantinfo.setMerchantName("New merchant 007");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Contact No", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void updatemerchant_info_blank_dynamicrouting_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("testaddress1");
		merchantinfo.setContactMail("test111@test.com");
		merchantinfo.setContactNumber("9786541236");
		merchantinfo.setIsDynamicRoutingEnabled("");
		merchantinfo.setMerchantCode("jBKP07OX");
		merchantinfo.setMerchantKey("nlClVJ");
		merchantinfo.setMerchantLogo("logonext");
		merchantinfo.setMerchantName("New merchant 007");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void updatemerchant_info_blank_merchantcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("testaddress1");
		merchantinfo.setContactMail("test111@test.com");
		merchantinfo.setContactNumber("9786541236");
		merchantinfo.setIsDynamicRoutingEnabled("true");
		merchantinfo.setMerchantCode("");
		merchantinfo.setMerchantKey("nlClVJ");
		merchantinfo.setMerchantLogo("logonext");
		merchantinfo.setMerchantName("New merchant 007");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void updatemerchant_info_invalid_merchantcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("testaddress1");
		merchantinfo.setContactMail("test111@test.com");
		merchantinfo.setContactNumber("9786541236");
		merchantinfo.setIsDynamicRoutingEnabled("true");
		merchantinfo.setMerchantCode("testinvalidcode");
		merchantinfo.setMerchantKey("nlClVJ");
		merchantinfo.setMerchantLogo("logonext");
		merchantinfo.setMerchantName("New merchant 007");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void updatemerchant_info_blank_merchantkey_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("testaddress1");
		merchantinfo.setContactMail("test111@test.com");
		merchantinfo.setContactNumber("9786541236");
		merchantinfo.setIsDynamicRoutingEnabled("true");
		merchantinfo.setMerchantCode("jBKP07OX");
		merchantinfo.setMerchantKey("");
		merchantinfo.setMerchantLogo("logonext");
		merchantinfo.setMerchantName("New merchant 007");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void updatemerchant_info_invalid_merchantkey_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("testaddress1");
		merchantinfo.setContactMail("test111@test.com");
		merchantinfo.setContactNumber("9786541236");
		merchantinfo.setIsDynamicRoutingEnabled("true");
		merchantinfo.setMerchantCode("jBKP07OX");
		merchantinfo.setMerchantKey("invalidmerchantkey");
		merchantinfo.setMerchantLogo("logonext");
		merchantinfo.setMerchantName("New merchant 007");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void updatemerchant_info_blank_merchantlogo_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("testaddress1");
		merchantinfo.setContactMail("test111@test.com");
		merchantinfo.setContactNumber("9786541236");
		merchantinfo.setIsDynamicRoutingEnabled("true");
		merchantinfo.setMerchantCode("jBKP07OX");
		merchantinfo.setMerchantKey("nlClVJ");
		merchantinfo.setMerchantLogo("");
		merchantinfo.setMerchantName("New merchant 007");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void updatemerchant_info_blank_merchantname_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantInfoVO merchantinfo = new MerchantInfoVO();
		merchantinfo.setAddress("testaddress1");
		merchantinfo.setContactMail("test111@test.com");
		merchantinfo.setContactNumber("9786541236");
		merchantinfo.setIsDynamicRoutingEnabled("true");
		merchantinfo.setMerchantCode("jBKP07OX");
		merchantinfo.setMerchantKey("nlClVJ");
		merchantinfo.setMerchantLogo("logonext");
		merchantinfo.setMerchantName("");
		
		PanelGenericResponse resp = client.updateMerchantInfo(merchantinfo, "jBKP07OX");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	@AfterTest
	public void Aftertest(){
		
		results.extreports.flush();
	}
	
}