package com.freecharge.klickpay.tests.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.node.BigIntegerNode;
import com.freecharge.core.Testing_Environment;
import com.freecharge.core.Util;
import com.freecharge.core.KlickPayCodeConstants;
import com.freecharge.ims.migration.core.IMSUtil;
import com.freecharge.ims.migration.core.RequestUtil;
import com.freecharge.ims.migration.core.Results;
import com.freecharge.klickpay.client.KlickpayClient;
import com.freecharge.klickpay.vo.MerchantConfigVO;
import com.freecharge.klickpay.vo.MerchantDetailsVO;
import com.freecharge.klickpay.vo.MerchantOptionsVO;
import com.freecharge.klickpay.vo.MerchantPGConfigVO;
import com.freecharge.klickpay.vo.MerchantSCConfigVO;
import com.freecharge.klickpay.vo.MerchantVO;
import com.freecharge.klickpay.vo.PanelGenericResponse;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class UpdateMerchantSCMapping_UseCases  {
	
	
	KlickpayClient client = new KlickpayClient("https://pay-dev.freecharge.in", 50000, 50000, "FC123", "AFADXCZA");
	Results results = new Results();
	RequestUtil requtil = new RequestUtil();
	
	@Test(enabled  = true)
	public void updatemerchantsc_config_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		merchantscconfig.setAccessKey("updated_newqa_key111");
		merchantscconfig.setSecret("updated_newqa_secret111");
		
		PanelGenericResponse resp = client.updateMerchantSCMapping(merchantscconfig, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void updatemerchantsc_config_invalid_merchantcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		merchantscconfig.setAccessKey("newqa_key111");
		merchantscconfig.setSecret("newqa_secret111");
		
		PanelGenericResponse resp = client.createMerchantSCMapping(merchantscconfig, "invalidtest");
		//System.out.println(resp.getStatus()+","+resp.getStatusMssg());
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Merchant Code", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void updatemerchantsc_config_blank_merchantcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantSCConfigVO merchantscconfig = new MerchantSCConfigVO();
		merchantscconfig.setAccessKey("newqa_key111");
		merchantscconfig.setSecret("newqa_secret111");
		
		PanelGenericResponse resp = client.createMerchantSCMapping(merchantscconfig, "");
		//System.out.println(resp.getStatus()+","+resp.getStatusMssg());
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Merchant Code", "Status Message is not displayed");
		
	}
	
	
	
	@AfterTest
	public void Aftertest(){
		
		results.extreports.flush();
	}
	
}