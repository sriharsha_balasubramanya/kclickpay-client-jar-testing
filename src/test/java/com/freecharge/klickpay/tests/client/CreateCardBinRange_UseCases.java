package com.freecharge.klickpay.tests.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.node.BigIntegerNode;
import com.freecharge.core.Testing_Environment;
import com.freecharge.core.Util;
import com.freecharge.core.KlickPayCodeConstants;
import com.freecharge.ims.migration.core.IMSUtil;
import com.freecharge.ims.migration.core.RequestUtil;
import com.freecharge.ims.migration.core.Results;
import com.freecharge.klickpay.client.KlickpayClient;
import com.freecharge.klickpay.vo.CardBinRangeVO;
import com.freecharge.klickpay.vo.MerchantConfigVO;
import com.freecharge.klickpay.vo.MerchantDetailsVO;
import com.freecharge.klickpay.vo.MerchantOptionsVO;
import com.freecharge.klickpay.vo.MerchantPGConfigVO;
import com.freecharge.klickpay.vo.MerchantSCConfigVO;
import com.freecharge.klickpay.vo.MerchantVO;
import com.freecharge.klickpay.vo.PanelGenericResponse;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class CreateCardBinRange_UseCases  {
	
	
	KlickpayClient client = new KlickpayClient("https://pay-dev.freecharge.in", 50000, 50000, "FC123", "AFADXCZA");
	Results results = new Results();
	RequestUtil requtil = new RequestUtil();
	
	@Test(enabled  = true)
	public void createcardbinrange_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		CardBinRangeVO cardbinrange = new CardBinRangeVO();
		cardbinrange.setBinHead("421388");
		cardbinrange.setBinTail("8774");
		cardbinrange.setIssuingBank("HDFC");
		cardbinrange.setRoutingSequence("testroute");
		cardbinrange.setCardType("AMEX");
		cardbinrange.setCardNature("DEBIT");
		
		
		PanelGenericResponse resp = client.createCardBinRange(cardbinrange);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createcardbinrange_blank_binhead_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		CardBinRangeVO cardbinrange = new CardBinRangeVO();
		cardbinrange.setBinHead("");
		cardbinrange.setBinTail("9846");
		cardbinrange.setIssuingBank("AXIS");
		cardbinrange.setRoutingSequence("testroute");
		cardbinrange.setCardType("VISA");
		cardbinrange.setCardNature("DEBIT");
		 
		
		PanelGenericResponse resp = client.createCardBinRange(cardbinrange);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid binhead provided.", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createcardbinrange_invalid_binhead_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		CardBinRangeVO cardbinrange = new CardBinRangeVO();
		cardbinrange.setBinHead("abcdinvalid");
		cardbinrange.setBinTail("9846");
		cardbinrange.setIssuingBank("AXIS");
		cardbinrange.setRoutingSequence("testroute");
		cardbinrange.setCardType("VISA");
		cardbinrange.setCardNature("DEBIT");
		
		
		PanelGenericResponse resp = client.createCardBinRange(cardbinrange);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid binhead provided.", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createcardbinrange_less_than_6digits_binhead_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		CardBinRangeVO cardbinrange = new CardBinRangeVO();
		cardbinrange.setBinHead("96880");
		cardbinrange.setBinTail("9846");
		cardbinrange.setIssuingBank("AXIS");
		cardbinrange.setRoutingSequence("testroute");
		cardbinrange.setCardType("VISA");
		cardbinrange.setCardNature("DEBIT");
		
		
		PanelGenericResponse resp = client.createCardBinRange(cardbinrange);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid binhead provided.", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createcardbinrange_more_than_6digits_binhead_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		CardBinRangeVO cardbinrange = new CardBinRangeVO();
		cardbinrange.setBinHead("9688059");
		cardbinrange.setBinTail("9846");
		cardbinrange.setIssuingBank("AXIS");
		cardbinrange.setRoutingSequence("testroute");
		cardbinrange.setCardType("VISA");
		cardbinrange.setCardNature("DEBIT");
		
		
		PanelGenericResponse resp = client.createCardBinRange(cardbinrange);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid binhead provided.", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createcardbinrange_blank_issuingbank_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		CardBinRangeVO cardbinrange = new CardBinRangeVO();
		cardbinrange.setBinHead("968805");
		cardbinrange.setBinTail("9846");
		cardbinrange.setIssuingBank("");
		cardbinrange.setRoutingSequence("testroute");
		cardbinrange.setCardType("VISA");
		cardbinrange.setCardNature("DEBIT");
		
		
		PanelGenericResponse resp = client.createCardBinRange(cardbinrange);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid issuing bank provided.", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createcardbinrange_invalid_issuingbank_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		CardBinRangeVO cardbinrange = new CardBinRangeVO();
		cardbinrange.setBinHead("968805");
		cardbinrange.setBinTail("9846");
		cardbinrange.setIssuingBank("123@@#!");
		cardbinrange.setRoutingSequence("testroute");
		cardbinrange.setCardType("VISA");
		cardbinrange.setCardNature("DEBIT");
		
		
		PanelGenericResponse resp = client.createCardBinRange(cardbinrange);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createcardbinrange_blank_cardtype_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		CardBinRangeVO cardbinrange = new CardBinRangeVO();
		cardbinrange.setBinHead("968805");
		cardbinrange.setBinTail("9846");
		cardbinrange.setIssuingBank("AXIS");
		cardbinrange.setRoutingSequence("testroute");
		cardbinrange.setCardType("");
		cardbinrange.setCardNature("DEBIT");
		
		
		PanelGenericResponse resp = client.createCardBinRange(cardbinrange);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Empty cardtype provided.", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createcardbinrange_invalid_cardtype_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		CardBinRangeVO cardbinrange = new CardBinRangeVO();
		cardbinrange.setBinHead("968805");
		cardbinrange.setBinTail("9846");
		cardbinrange.setIssuingBank("AXIS");
		cardbinrange.setRoutingSequence("testroute");
		cardbinrange.setCardType("TESTINVALID");
		cardbinrange.setCardNature("DEBIT");
		
		
		PanelGenericResponse resp = client.createCardBinRange(cardbinrange);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void createcardbinrange_blank_cardnature_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		CardBinRangeVO cardbinrange = new CardBinRangeVO();
		cardbinrange.setBinHead("968805");
		cardbinrange.setBinTail("9846");
		cardbinrange.setIssuingBank("AXIS");
		cardbinrange.setRoutingSequence("testroute");
		cardbinrange.setCardType("VISA");
		cardbinrange.setCardNature("");
		
		
		PanelGenericResponse resp = client.createCardBinRange(cardbinrange);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Empty card nature provided.", "Status Message is not displayed");
		
	}
	
	

	@Test(enabled  = true)
	public void createcardbinrange_invalid_cardnature_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		CardBinRangeVO cardbinrange = new CardBinRangeVO();
		cardbinrange.setBinHead("968805");
		cardbinrange.setBinTail("9846");
		cardbinrange.setIssuingBank("AXIS");
		cardbinrange.setRoutingSequence("testroute");
		cardbinrange.setCardType("VISA");
		cardbinrange.setCardNature("TESTINVALID");
		
		
		PanelGenericResponse resp = client.createCardBinRange(cardbinrange);
		System.out.println(resp);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@AfterTest
	public void Aftertest(){
		
		results.extreports.flush();
	}
	
}