package com.freecharge.klickpay.tests.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.node.BigIntegerNode;
import com.freecharge.core.Testing_Environment;
import com.freecharge.core.Util;
import com.freecharge.core.KlickPayCodeConstants;
import com.freecharge.ims.migration.core.IMSUtil;
import com.freecharge.ims.migration.core.RequestUtil;
import com.freecharge.ims.migration.core.Results;
import com.freecharge.klickpay.client.KlickpayClient;
import com.freecharge.klickpay.vo.MerchantConfigVO;
import com.freecharge.klickpay.vo.MerchantDetailsVO;
import com.freecharge.klickpay.vo.MerchantIPConfigVO;
import com.freecharge.klickpay.vo.MerchantOptionsVO;
import com.freecharge.klickpay.vo.MerchantPGConfigVO;
import com.freecharge.klickpay.vo.MerchantSCConfigVO;
import com.freecharge.klickpay.vo.MerchantVO;
import com.freecharge.klickpay.vo.PanelGenericResponse;
import com.freecharge.klickpay.vo.MerchantIPMapping.API_TYPE;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class CreateMerchantIPConfig_UseCases  {
	
	
	KlickpayClient client = new KlickpayClient("https://pay-dev.freecharge.in", 50000, 50000, "FC123", "AFADXCZA");
	Results results = new Results();
	RequestUtil requtil = new RequestUtil();
	
	@Test(enabled  = true)
	public void createmerchantip_config_card_eleigibility_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantIPConfigVO merchantipconfg = new MerchantIPConfigVO();
		merchantipconfg.setIps("52.82.110.95");
		merchantipconfg.setApiType(API_TYPE.CARD_ELIGIBILITY_API);
		
		PanelGenericResponse resp = client.createMerchantIPMapping(merchantipconfg, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createmerchantip_config_debit_atm_status_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantIPConfigVO merchantipconfg = new MerchantIPConfigVO();
		merchantipconfg.setIps("52.6.110.101");
		merchantipconfg.setApiType(API_TYPE.DEBIT_ATM_STATUS_API);
		
		PanelGenericResponse resp = client.createMerchantIPMapping(merchantipconfg, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createmerchantip_config_icici_qc_status_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantIPConfigVO merchantipconfg = new MerchantIPConfigVO();
		merchantipconfg.setIps("74.6.101.95");
		merchantipconfg.setApiType(API_TYPE.ICICI_QC_STATUS_API);
		
		
		PanelGenericResponse resp = client.createMerchantIPMapping(merchantipconfg, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void createmerchantip_config_payment_request_checksum_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantIPConfigVO merchantipconfg = new MerchantIPConfigVO();
		merchantipconfg.setIps("52.6.17.95");
		merchantipconfg.setApiType(API_TYPE.PAYMENT_REQUEST_CHECKSUM_API);
		
		
		PanelGenericResponse resp = client.createMerchantIPMapping(merchantipconfg, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createmerchantip_config_pg_performance_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantIPConfigVO merchantipconfg = new MerchantIPConfigVO();
		merchantipconfg.setIps("52.6.110.95");
		merchantipconfg.setApiType(API_TYPE.PG_PERFORMANCE_API);
		
		
		PanelGenericResponse resp = client.createMerchantIPMapping(merchantipconfg, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createmerchantip_refund_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantIPConfigVO merchantipconfg = new MerchantIPConfigVO();
		merchantipconfg.setIps("121.6.110.95");
		merchantipconfg.setApiType(API_TYPE.REFUND);
		
		
		PanelGenericResponse resp = client.createMerchantIPMapping(merchantipconfg, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createmerchantip_refund_request_chksum_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantIPConfigVO merchantipconfg = new MerchantIPConfigVO();
		merchantipconfg.setIps("52.6.87.95");
		merchantipconfg.setApiType(API_TYPE.REFUND_REQUEST_CHECKSUM_API);
		
		
		PanelGenericResponse resp = client.createMerchantIPMapping(merchantipconfg, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	
	@Test(enabled  = true)
	public void createmerchantip_status_chk_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantIPConfigVO merchantipconfg = new MerchantIPConfigVO();
		merchantipconfg.setIps("52.6.99.95");
		merchantipconfg.setApiType(API_TYPE.STATUS_CHECK);
		
		
		PanelGenericResponse resp = client.createMerchantIPMapping(merchantipconfg, "QClQi0XM");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E000", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Success !", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void createmerchantip_invalid_merchantcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantIPConfigVO merchantipconfg = new MerchantIPConfigVO();
		merchantipconfg.setIps("52.6.99.95");
		merchantipconfg.setApiType(API_TYPE.STATUS_CHECK);
		
		
		PanelGenericResponse resp = client.createMerchantIPMapping(merchantipconfg, "invalidmcode");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Merchant Code", "Status Message is not displayed");
		
	}
	
	@Test(enabled  = true)
	public void createmerchantip_blank_merchantcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		MerchantIPConfigVO merchantipconfg = new MerchantIPConfigVO();
		merchantipconfg.setIps("52.6.99.95");
		merchantipconfg.setApiType(API_TYPE.STATUS_CHECK);
		
		
		PanelGenericResponse resp = client.createMerchantIPMapping(merchantipconfg, "");
		//System.out.println(merchantvo.);
		results.assertequals(resp.getStatus(), "E500", "Status is not displayed");
		results.assertequals(resp.getStatusMssg(), "Invalid Merchant Code", "Status Message is not displayed");
		
	}
	
	
		
	@AfterTest
	public void Aftertest(){
		
		results.extreports.flush();
	}
	
}