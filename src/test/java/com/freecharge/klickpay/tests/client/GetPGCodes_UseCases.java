package com.freecharge.klickpay.tests.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.node.BigIntegerNode;
import com.freecharge.core.Testing_Environment;
import com.freecharge.core.Util;
import com.freecharge.core.KlickPayCodeConstants;
import com.freecharge.ims.migration.core.IMSUtil;
import com.freecharge.ims.migration.core.RequestUtil;
import com.freecharge.ims.migration.core.Results;
import com.freecharge.klickpay.client.KlickpayClient;
import com.freecharge.klickpay.vo.CardBinRangeVO;
import com.freecharge.klickpay.vo.MerchantConfigVO;
import com.freecharge.klickpay.vo.MerchantDetailsVO;
import com.freecharge.klickpay.vo.MerchantInfoVO;
import com.freecharge.klickpay.vo.MerchantOptionsVO;
import com.freecharge.klickpay.vo.MerchantPGConfigVO;
import com.freecharge.klickpay.vo.MerchantSCConfigVO;
import com.freecharge.klickpay.vo.MerchantVO;
import com.freecharge.klickpay.vo.PGCodesVO;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class GetPGCodes_UseCases  {
	
	
	KlickpayClient client = new KlickpayClient("https://pay-dev.freecharge.in", 50000, 50000, "FC123", "AFADXCZA");
	Results results = new Results();
	RequestUtil requtil = new RequestUtil();
	
	@Test(enabled  = true)
	public void getpgcodes_SuccessUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		
		PGCodesVO pgcodes = client.getPGCodes("QClQi0XM", "cca", "AMEX", "VISA");
		
		System.out.println(pgcodes);
		results.assertnotnull(pgcodes.getCardType(), "Card type is not empty");
		results.assertnotnull(pgcodes.getIsActive(), "Active status is not empty");
		results.assertnotnull(pgcodes.getPaymentTypeCode(), "Payment type code is not empty");
		results.assertnotnull(pgcodes.getPgBankCode(), "PG bank code is not empty");
		results.assertnotnull(pgcodes.getPgCode(), "PG Code is not empty");
		results.assertnotnull(pgcodes.getPgItemCode(), "PG item code is not empty");
	}
	
	@Test(enabled  = true)
	public void getpgcodes_blank_merchantcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		
		PGCodesVO pgcodes = client.getPGCodes("", "cca", "AMEX", "VISA");
		
		System.out.println(pgcodes);
		results.assertequals(pgcodes.getStatus(), "E500", "Status code is not displayed properly");
		results.assertequals(pgcodes.getStatusMssg(), "Invalid or empty input provided.", "Status Message is not displayed properly");
	}
	
	@Test(enabled  = true)
	public void getpgcodes_invalid_merchantcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		
		PGCodesVO pgcodes = client.getPGCodes("invalidmcode", "cca", "AMEX", "VISA");
		
		System.out.println(pgcodes);
		results.assertequals(pgcodes.getStatus(), "E500", "Status code is not displayed properly");
		results.assertequals(pgcodes.getStatusMssg(), "Invalid Merchant Code", "Status Message is not displayed properly");
	}
	
	@Test(enabled  = true)
	public void getpgcodes_blank_pgcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		
		PGCodesVO pgcodes = client.getPGCodes("QClQi0XM", "", "AMEX", "VISA");
		
		System.out.println(pgcodes);
		results.assertequals(pgcodes.getStatus(), "E500", "Status code is not displayed properly");
		results.assertequals(pgcodes.getStatusMssg(), "Invalid or empty input provided.", "Status Message is not displayed properly");
	}
	
	@Test(enabled  = true)
	public void getpgcodes_invalid_pgcode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		
		PGCodesVO pgcodes = client.getPGCodes("QClQi0XM", "invalipgcode", "AMEX", "VISA");
		
		System.out.println(pgcodes);
		results.assertequals(pgcodes.getStatus(), "E500", "Status code is not displayed properly");
		results.assertequals(pgcodes.getStatusMssg(), "No PG Code found for given input parameters pgCode:invalipgcode paymentTypeCode:AMEX cardType:VISA", "Status Message is not displayed properly");
	}
	
	@Test(enabled  = true)
	public void getpgcodes_blank_paymenttypecode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		
		PGCodesVO pgcodes = client.getPGCodes("QClQi0XM", "cca", "", "VISA");
		
		System.out.println(pgcodes);
		results.assertequals(pgcodes.getStatus(), "E500", "Status code is not displayed properly");
		results.assertequals(pgcodes.getStatusMssg(), "Invalid or empty input provided.", "Status Message is not displayed properly");
	}
	
	@Test(enabled  = true)
	public void getpgcodes_invalid_paymenttypecode_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		
		PGCodesVO pgcodes = client.getPGCodes("QClQi0XM", "cca", "invalidpaymentcode", "VISA");
		
		System.out.println(pgcodes);
		results.assertequals(pgcodes.getStatus(), "E500", "Status code is not displayed properly");
		results.assertequals(pgcodes.getStatusMssg(), "No PG Code found for given input parameters pgCode:cca paymentTypeCode:invalidpaymentcode cardType:VISA", "Status Message is not displayed properly");
	}
	
	@Test(enabled  = true)
	public void getpgcodes_blank_cardtype_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		
		PGCodesVO pgcodes = client.getPGCodes("QClQi0XM", "cca", "AMEX", "");
		
		System.out.println(pgcodes);
		results.assertequals(pgcodes.getStatus(), "E500", "Status code is not displayed properly");
		results.assertequals(pgcodes.getStatusMssg(), "Invalid or empty input provided.", "Status Message is not displayed properly");
	}
	
	@Test(enabled  = true)
	public void getpgcodes_invalid_cardtype_FailureUseCase() throws Exception {
		results.createtestcase(Thread.currentThread().getStackTrace()[1].getMethodName(), this.getClass().getSimpleName());
		
		
		PGCodesVO pgcodes = client.getPGCodes("QClQi0XM", "pg123", "abc123", "invalidcardtype");
		
		System.out.println(pgcodes);
		results.assertequals(pgcodes.getStatus(), "E500", "Status code is not displayed properly");
		results.assertequals(pgcodes.getStatusMssg(), "No PG Code found for given input parameters pgCode:pg123 paymentTypeCode:abc123 cardType:invalidcardtype", "Status Message is not displayed properly");
	}
	
	
	@AfterTest
	public void Aftertest(){
		
		results.extreports.flush();
	}
	
}