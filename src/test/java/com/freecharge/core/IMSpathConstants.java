package com.freecharge.core;

/**
 * Manintain the API endpoints used.
 * @author sriharsha
 *
 */


public class IMSpathConstants {
	   public static final String CREATE_SESSION_PATH = "/rest/session/create";
	   public static final String CREATE_USER_WITH_EMAIL="/api/v2/identity/create/user/email";
	   public static final String VERIFY_USER="/api/v2/identity/verify/user/mobile";
	   public static final String SIGN_IN_USER_WITH_EMAIL="/api/v2/identity/signin/user";

	 
       
}
