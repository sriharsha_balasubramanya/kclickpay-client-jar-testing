package com.freecharge.core;
/**
 * Maintain all the URL path for the MOB
 * @author admin_2
 *
 */
public class KlickPayCodeConstants {

	   public static final String [] PAYMENT_TYPE_CODES = {"DC", "CC", "NB", "ATM", "CCD", "EMI", "SC","OCW", "AMEX", "DINR", "EZEC"};
	   public static final String [] CARD_TYPE_CODES = {"VI", "MC", "MAES", "MAES_S", "RPAY"};
	   public static final String [] PG_CODES = {"cca", "bdk", "icici", "payu", "ocw", "fss", "ctp", "hdfc"};

       
} 
